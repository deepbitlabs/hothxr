using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Microsoft.MixedReality;
using UnityEngine.EventSystems;
using UnityEngine.Events;
using UnityEngine.UI;
using Microsoft.MixedReality.Toolkit.Input;

public class itemGrab : MonoBehaviour, IMixedRealityTouchHandler
{
    private GameObject grabbableObject;
    // Start is called before the first frame update
    void Start()
    {
        grabbableObject = this.gameObject;
        MakeNearDraggable(grabbableObject);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void OnTouchStarted(HandTrackingInputEventData eventData)
    {
        string ptrName = eventData.InputSource.Pointers[0].PointerName;
        Debug.Log($"Touch started from {ptrName}");
    }

    public void OnTouchCompleted(HandTrackingInputEventData eventData)
    {
        throw new System.NotImplementedException();
    }

    public void OnTouchUpdated(HandTrackingInputEventData eventData)
    {
        throw new System.NotImplementedException();
    }

    public static void MakeNearDraggable(GameObject target)
    {
        target.AddComponent<NearInteractionGrabbable>();
        var pointerHandler = target.AddComponent<PointerHandler>();
        pointerHandler.OnPointerDown.AddListener((e) =>
        {
            if (e.Pointer is SpherePointer)
            {
                target.transform.parent = (((SpherePointer)e.Pointer)).transform;
            }
        });
        pointerHandler.OnPointerUp.AddListener((e) =>
        {
            if (e.Pointer is SpherePointer)
            {
                target.transform.parent = null;
            }
        });
    }
}
