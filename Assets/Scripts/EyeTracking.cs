using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Microsoft.MixedReality.Toolkit;

public class EyeTracking : MonoBehaviour
{
    //GameObjects Used for Eye Tracking
    public GameObject brain;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void PlaceOnGaze()
    {
        brain.transform.position = CoreServices.InputSystem.EyeGazeProvider.HitPosition;
    }
}
