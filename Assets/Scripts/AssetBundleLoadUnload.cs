using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class AssetBundleLoadUnload : MonoBehaviour
{
    public GameObject prefabLocation;

    [Header("Do not chance any Settings below during the testing phase")]
    public bool testing = true; //do not chance this setting until we are ready to shift to an AssetBundle in the cloud.


    IEnumerator Start()
    {
        if (!testing)
        {
            var uwr = UnityWebRequestAssetBundle.GetAssetBundle("url string here");
            yield return uwr.SendWebRequest();

            //get a specific asset from the bundle and instantiate it
            AssetBundle bundle = DownloadHandlerAssetBundle.GetContent(uwr);
            var loadAsset = bundle.LoadAssetAsync<GameObject>("AssetsFolder/ClientsFolder/Client00001/scan.prefab");
            yield return loadAsset;

            Instantiate(loadAsset.asset, new Vector3(prefabLocation.transform.position.x, prefabLocation.transform.position.y, prefabLocation.transform.position.z), Quaternion.identity);
        }
    }
}
