using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Windows.Speech;
using System.Linq;

public class PhraseRecognition : MonoBehaviour
{
    KeywordRecognizer keywordRecognizer;
    Dictionary<string, System.Action> keywords = new Dictionary<string, System.Action>();

    Brain brain = new Brain();
    EyeTracking eT = new EyeTracking();
    GameManager gm = new GameManager();
    Placement placement = new Placement();
    public GameObject MeasurementWindow;
    AIAssistant AI = new AIAssistant();


    // Start is called before the first frame update
    void Start()
    {
        //create keyword library for phraserecognition
        CreateKeywordLibrary();

        
        
        keywordRecognizer = new KeywordRecognizer(keywords.Keys.ToArray());
        keywordRecognizer.OnPhraseRecognized += KeywordRecognizer_OnPhraseRecognized;

        
        keywordRecognizer.Start();
    }

    private void KeywordRecognizer_OnPhraseRecognized(PhraseRecognizedEventArgs args)
    {
        System.Action LockBrain;
        //if the keyword recognized is in the dictionary, call the action

        if (keywords.TryGetValue(args.text, out LockBrain))
        {
            LockBrain.Invoke();
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void CreateKeywordLibrary()
    {
        keywords.Add("activate", () =>
        {
            //activate initialization of program

            //confirm here that the hololens is working correctly and that there is currently a 3D model set up. If none are setup display an error message.

        });
        keywords.Add("Lock Brain", () =>
        {
            // lock brain function
            brain.PinObject();
        });
        keywords.Add("Unlock Brain", () =>
        {
            //unlock brain function
            brain.MoveObject();
        });
        keywords.Add("Start Diction Note", () =>
        {
            //start dictating
            gm.DictateToFile();

        });
        keywords.Add("Stop Dictation", () =>
        {
            //stop dicating
            gm.StopDictation();

        });
        keywords.Add("Gaze", () =>
        {
            //place brain on keyword "Gaze"
            eT.PlaceOnGaze();
        });
        keywords.Add("Spawn Brain", () =>
        {
            //spawn a brain 3d object that is grabbable and scaleable
            gm.SpawnBrain();
        });
        keywords.Add("Placement Test", () =>
        {
            //for ML development -- !!!THIS WILL NOT WORK CORRECTLY WITHOUT ML CODE ADDED INTO Placement.cs FILE!!!
            placement.ObjPlacement();
        });
        keywords.Add("Measurement Window Open", () =>
        {
            MeasurementWindow.SetActive(true);
        });
        keywords.Add("Measurement Window Close", () =>
        {
            MeasurementWindow.SetActive(false);
        });
        keywords.Add("Start Up Assistant", () =>
        {
            AI.StartUpAssistant();
        });
        keywords.Add("Turn Off Assistant", () =>
        {
            AI.TurnOffAssistant();
        });
        keywords.Add("Choose Assistant", () =>
        {
            AI.ChooseAssistant();
        });

    }
}
