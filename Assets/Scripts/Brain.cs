using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Brain : MonoBehaviour
{
    public GameObject brainScan;
    
    [HideInInspector]
    public Rigidbody brainBody;



    // Start is called before the first frame update
    void Start()
    {
        brainBody = brainScan.GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void PinObject()
    {
        brainBody.constraints = RigidbodyConstraints.FreezeAll;
    }

    public void MoveObject()
    {
        brainBody.constraints = RigidbodyConstraints.None;
    }


}
