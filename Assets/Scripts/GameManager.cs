using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Microsoft.MixedReality;

public class GameManager : MonoBehaviour
{
    public GameObject Brain;
    
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void UserMove()
    {
        
        

    }

    public void UserGaze()
    {
        
    }
    public void DictateToFile()
    {
        //add diction in future for doctor notes
    }

    public void StopDictation()
    {
        // stop dictation to file from DictationToFile() method.
    }

    public void SpawnBrain()
    {
        Instantiate(Brain, new Vector3(1,1,1),Quaternion.identity);
    }
}
