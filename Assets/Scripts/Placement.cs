using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Microsoft.MixedReality.Toolkit;
using UnityEngine.UI;
using System.Globalization;
using UnityEngine.TextCore;
using TMPro;

public class Placement : MonoBehaviour
{
    //General Variables
    [Header("General Variables")]
    public GameObject brainObj;
    [HideInInspector]
    public Transform[] anchors;
    private bool isPlaced = false;
    private Transform currentAnchor;
    public Vector3 visualOffset;

    //Measurements
    [Header("Measurement's -- Used in ML")]
    public GameObject PointOfOrigin; //this is where all the measurements below are based from
    public float rightEye;
    public float leftEye;
    public float noseTip;
    public float rightEyeBrow;
    public float leftEyeBrow;
    public float skullThickness;
    public float leftEar;
    public float rightEar;

    //UIMeasurementData
    [Header("Input Fields for UI Values")]
    public TMP_InputField rightEyeValue;
    public TMP_InputField leftEyeValue;
    public TMP_InputField noseTipValue;
    public TMP_InputField rightEyeBrowValue;
    public TMP_InputField leftEyeBrowValue;
    public TMP_InputField skullThicknessValue;
    public TMP_InputField leftEarValue;
    public TMP_InputField rightEarValue;

    //Anchors
    [Header("Anchors")]
    public GameObject rightEyePos;
    public GameObject leftEyePos;
    public GameObject noseTipPos;
    public GameObject rightEyeBrowPos;
    public GameObject leftEyeBrowPos;
    public GameObject skullThicknessPos;
    public GameObject leftEarPos;
    public GameObject rightEarPos;

    //ComputerCalculatedPositions
    [HideInInspector]
    public Transform _rightEye;
    [HideInInspector]
    public Transform _leftEye;
    [HideInInspector]
    public Transform _noseTip;
    [HideInInspector]
    public Transform _rightEyeBrow;
    [HideInInspector]
    public Transform _leftEyeBrow;
    [HideInInspector]
    public Transform _skullThickness;
    [HideInInspector]
    public Transform _leftEar;
    [HideInInspector]
    public Transform _rightEar;


    // Start is called before the first frame update
    void Start()
    {
        GetAnchorInformation();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void ObjPlacement()
    {
        PlacementCalculations();
        RaycastForML();

    }

    public void PlacementCalculations()
    {
        //update the anchors
        rightEyePos.transform.position = FindRightEye();
        leftEyePos.transform.position = FindLeftEye();
        noseTipPos.transform.position = FindNoseTip();
        rightEyeBrowPos.transform.position = FindRightBrow();
        leftEyeBrowPos.transform.position = FindLeftBrow();
        skullThicknessPos.transform.position = FindSkullThicknessAnchor();
    }

    /*
     * For the next sections of code in the methods named "Find____ _____"
     * 
     * 1. Create an ML script to find each position and grab the XYZ coods of the center of each object the computer needs to find IE: for the right eye we need the XYZ coods of the middle of the right eye.
     * 2. After getting those XYZ coords set the coordinates as the Vector3 that is created in each method.
     * 3. After we retuern that Vector3 position, we can update the anchors to where they need to be then do some math for the offset based on where the user is looking at the patient from.
     * -- After we have the ML script attached we can make small edits in the editor to the "PointofOrigin" and "VisualOffset" variables until it is lined up corrected where we need it to be for the test.
     */




    public Vector3 FindRightEye()
    {
        //insert ML here for finding right eye

        //testscript
        Vector3 rEye = new Vector3(_rightEye.transform.position.x, _rightEye.transform.position.y, _rightEye.transform.position.z);

        //returnvalue
        return rEye;
    }

    public Vector3 FindLeftEye()
    {
        //insert ML here for finding right eye

        //testscript
        Vector3 lEye = new Vector3(_leftEye.transform.position.x, _leftEye.transform.position.y, _leftEye.transform.position.z);

        //returnvalue
        return lEye;
    }

    public Vector3 FindNoseTip()
    {
        //insert ML here for finding right eye

        //testscript
        Vector3 nTip = new Vector3(_noseTip.transform.position.x, _noseTip.transform.position.y, _noseTip.transform.position.z);

        //returnvalue
        return nTip;
    }

    public Vector3 FindRightBrow()
    {
        //insert ML here for finding right eye

        //testscript
        Vector3 rBrow = new Vector3(_rightEyeBrow.transform.position.x, _rightEyeBrow.transform.position.y, _rightEyeBrow.transform.position.z);

        //returnvalue
        return rBrow;
    }

    public Vector3 FindLeftBrow()
    {
        //insert ML here for finding right eye

        //testscript
        Vector3 lBrow = new Vector3(_leftEyeBrow.transform.position.x, _leftEyeBrow.transform.position.y, _leftEyeBrow.transform.position.z);

        //returnvalue
        return lBrow;
    }

    public Vector3 FindSkullThicknessAnchor()
    {
        //insert ML here for finding right eye

        //testscript
        Vector3 sThickness = new Vector3(_skullThickness.transform.position.x, _skullThickness.transform.position.y, _skullThickness.transform.position.z);

        //returnvalue
        return sThickness;
    }

    public void RaycastForML()
    {
        RaycastHit hit;
        Ray ray = Camera.main.ScreenPointToRay(CoreServices.InputSystem.EyeGazeProvider.HitPosition);

        if (Physics.Raycast (ray, out hit))
        {
            if (!brainObj)
            {
                brainObj = (GameObject)Instantiate(brainObj, hit.point + visualOffset, Quaternion.identity);                
            }
            else if (!isPlaced)
            {
                foreach (Transform anchor in anchors)
                {
                    if (!currentAnchor)
                    {
                        brainObj.transform.position = hit.point + visualOffset;
                    }
                }

            }
            else
            {
                Debug.Log("Error909: Brain will not place, an error has occurred.");
            }

            
        }
    }

    public void GetAnchorInformation()
    {
        GameObject[] g = GameObject.FindGameObjectsWithTag("Anchor");
        anchors = new Transform[g.Length];
        for (int i = 0; i < anchors.Length; i++)
        {
            anchors[i] = g[i].transform;
        }
    }

    public void UIPlacementDataSave()
    {
        rightEye = float.Parse(rightEyeValue.text);
        leftEye = float.Parse(leftEyeValue.text);
        noseTip = float.Parse(noseTipValue.text);
        rightEyeBrow = float.Parse(rightEyeBrowValue.text);
        leftEyeBrow = float.Parse(leftEyeBrowValue.text);
        skullThickness = float.Parse(skullThicknessValue.text);
        leftEar = float.Parse(leftEarValue.text);
        rightEar = float.Parse(rightEarValue.text);
    }
}
